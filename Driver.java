package arrayobj.car.uber;

import java.time.LocalTime;

public class Driver {
   private String driverId;
    private String driverName;
    private String carModelNo;
    private String driverLocation;
    private float driverRating;
    private LocalTime arrivalTime = LocalTime.now().plusMinutes(30);

    public Driver(String driverId, String driverName, String carModelNo, String driverLocation, float driverRating) {
        this.driverId=driverId;
        this.driverName = driverName;
        this.carModelNo = carModelNo;
        this.driverLocation = driverLocation;
        this.driverRating = driverRating;
    }

    public LocalTime getArrivalTime() {
        return arrivalTime;
    }

    public void setArrivalTime(LocalTime arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getCarModelNo() {
        return carModelNo;
    }

    public void setCarModelNo(String carModelNo) {
        this.carModelNo = carModelNo;
    }

    public String getDriverLocation() {
        return driverLocation;
    }

    public void setDriverLocation(String driverLocation) {
        this.driverLocation = driverLocation;
    }

    public float getDriverRating() {
        return driverRating;
    }

    public void setDriverRating(float driverRating) {
        this.driverRating = driverRating;
    }

}
