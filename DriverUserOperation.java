package arrayobj.car.uber;
import static arrayobj.car.uber.MainOperation.*;
import static arrayobj.car.uber.DriverService.*;
import static arrayobj.car.uber.Driver.*;
import static arrayobj.car.uber.RiderService.*;

import java.time.LocalTime;
import java.util.Scanner;

public class DriverUserOperation {
    static Scanner scanner = new Scanner(System.in);


    static Driver registerDriver(){
        scanner.nextLine();
        System.out.println("Enter Driver ID : ");
        String driverId = scanner.nextLine();
        System.out.println("Enter Driver Name :");
        String driverName = scanner.nextLine();
        System.out.println("Enter Driver Car Model No. :");
        String carModelNo = scanner.nextLine();
        System.out.println("Enter Driver Location :");
        String driverLocation = scanner.nextLine();
        System.out.println("Enter Driver Rating :");
        Float driverRating = scanner.nextFloat();

        return new Driver(driverId,driverName,carModelNo,driverLocation,driverRating);
    }

    static void deleteDriver(){
        System.out.println("Enter Driver ID which you want to Delete : ");
        scanner.nextLine();
        String driverIdToDelete = scanner.nextLine();

        System.out.println(driverIdToDelete);

        for (int i =0; i < dIndex; i++){
            System.out.println("i loop");
            if(drivers[i].getDriverId().equals(driverIdToDelete)){
                System.out.println("if ok");
                for (int j =i; j<dIndex; j++){
                    System.out.println("j loop");
                    drivers[j] = drivers[j+1];
                }
                System.out.println("Deleted successfully");
                dIndex = dIndex - 1;
                break;
            }

        }
        if(dIndex > 0) {
            for (int i = 0; i < dIndex; i++) {
                System.out.println("Driver ID : " + drivers[i].getDriverId());
                System.out.println("Driver Name : " + drivers[i].getDriverName());
                System.out.println("Driver Car Model No. : " + drivers[i].getDriverLocation());
                System.out.println("Driver Location : " + drivers[i].getDriverLocation());
                System.out.println("Driver Rating : " + drivers[i].getDriverRating());
                System.out.println("-----------------------------");

            }
        }
    }

    public static void acceptBooking(){
        scanner.nextLine();
        System.out.println("\nPlease Enter rider ID: ");
        String riderId = scanner.nextLine();

        System.out.println("\nPlease Enter your Driver Id: ");
        String driverId = scanner.nextLine();

        for (int i =0; i<rIndex ; i++){
            if (riders[i].getRiderId().equals(riderId)){
                riders[i].IsBooked = "Booked";
                riders[i].driverId = driverId;
                for (int j = 0; j<dIndex; j++){
                    if (drivers[j].getDriverId().equals(driverId)){
                        riders[i].driverName = drivers[j].getDriverName();
                        break;
                    }
                }
                System.out.println("Estimate Arrival Time : " + LocalTime.now().plusMinutes(30));
                System.out.println("Booking Accepted for rider: "+ riders[i].getRiderName());
                break;
            }
        }


    }
    static void driverFunctionality() {
        int choice;

        while (true){
            System.out.println("\nPress 1: for Register yourself as a Driver\n " +
                    "Press 2: for delete yourself from Driver List\n" +
                    "Press 3: For Display All Drivers\n " +
                    "Press 4: For view booking\n" +
                    "Press 5: Accept Rider booking\n" +
                    "Press 6: for Go Back to Main Menu\n");
            System.out.println("------------------------------------------");
            System.out.println("Enter Your Choice");
             choice = scanner.nextInt();

             switch (choice){
                 case 1:
                     Driver driver = registerDriver();
                     addDriver(driver);
                     break;
                 case 2:
                     deleteDriver();
                     break;
                 case 3:
                     getAllDriver();
                 case 4:
                     getAllRiders();
                     break;
                 case 5:
                     acceptBooking();
                     break;
                 case 6:
                     allOptions();
                     break;
             }
        }

    }

}
