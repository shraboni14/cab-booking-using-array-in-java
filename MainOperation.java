package arrayobj.car.uber;
import java.util.Scanner;

import static arrayobj.car.uber.DriverUserOperation.*;
import static arrayobj.car.uber.RiderUserOperation.*;

public class MainOperation {
    static Scanner scanner = new Scanner(System.in);

    public static void allOptions(){
        int choice;
        while (true){
            System.out.println("\nPress 1: For Driver Panel \n " +
                    "Press 2: For Rider Panel \n " +
                    "Press 3: For Quit");
            System.out.println("--------------------------------");
            System.out.println("Enter Your Choice");
            choice = scanner.nextInt();
            switch (choice){
                case 1:
                    driverFunctionality();
                    break;
                case 2:
                    riderFunctionality();
                    break;
                case 3:
                    System.exit(0);
            }
        }
    }
    public static void main(String[] args) {
        System.out.println("------------Welcome to the CAB Booking System-----------");
        allOptions();

    }

}
