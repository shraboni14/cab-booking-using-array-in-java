package arrayobj.car.uber;
import java.time.LocalTime;
public class Rider {
    String riderId;
    String riderName;
    String riderSourceLocation;
    String riderDestinationLocation;
    LocalTime bookingTime = LocalTime.now();
    String IsBooked;
    String driverId;
    String driverName;

    public Rider(String riderId,String riderName, String riderSourceLocation, String riderDestinationLocation, String IsBooked, String driverId, String driverName) {
        this.riderId = riderId;
        this.riderName = riderName;
        this.riderSourceLocation = riderSourceLocation;
        this.riderDestinationLocation = riderDestinationLocation;
        this.IsBooked = IsBooked;
        this.driverId = driverId;
        this.driverName = driverName;

    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getRiderId() {
        return riderId;
    }

    public void setRiderId(String riderId) {
        this.riderId = riderId;
    }

    public String getRiderName() {
        return riderName;
    }

    public void setRiderName(String riderName) {
        this.riderName = riderName;
    }

    public String getRiderSourceLocation() {
        return riderSourceLocation;
    }

    public void setRiderSourceLocation(String riderSourceLocation) {
        this.riderSourceLocation = riderSourceLocation;
    }

    public String getRiderDestinationLocation() {
        return riderDestinationLocation;
    }

    public void setRiderDestinationLocation(String riderDestinationLocation) {
        this.riderDestinationLocation = riderDestinationLocation;
    }

    public LocalTime getBookingTime() {
        return bookingTime;
    }

    public void setBookingTime(LocalTime bookingTime) {
        this.bookingTime = bookingTime;
    }

    public void setIsBooked(String IsBooked){
        this.IsBooked = IsBooked;
    }

    public String getIsBooked(){
        return IsBooked;
    }

    public void setDriverId(String driverId){
        this.driverId = driverId;
    }

    public String getDriverId(){
        return driverId;
    }
}
