package arrayobj.car.uber;
import java.time.LocalTime;
import static arrayobj.car.uber.Rider.*;
import static arrayobj.car.uber.Driver.*;
public class RiderService {
   static Rider[] riders = new Rider[10];

   static int rIndex;

   public static void addRider(Rider rider){
       riders[rIndex] = rider;
       rIndex++;
      System.out.println("Cab Booked Successfully, Wait for sometime");
      System.out.println("Your Booking Time : " + LocalTime.now());
//      System.out.println("Cab estimate arrival time : "+ LocalTime.now().plusMinutes(30));
      System.out.println("----------------------------------------");
   }
   static void getAllRiders(){
      for (int i =0; i<rIndex; i++){
         System.out.println("Rider ID : " + riders[i].getRiderId());
         System.out.println("Rider Name :" + riders[i].getRiderName());
         System.out.println("Rider Source Location :" + riders[i].getRiderSourceLocation());
         System.out.println("Rider Destination Location :" + riders[i].getRiderDestinationLocation());;
         System.out.println("Cab estimate arrival time : " );
         System.out.println("Your Booking Time : " + riders[i].getBookingTime());
         System.out.println("Is Accepted by driver: "+ riders[i].getIsBooked());
         System.out.println("Assigned Driver ID: "+ riders[i].getDriverId());
         System.out.println("Assigned Driver Name: "+ riders[i].getDriverName());
         System.out.println("-------------------------------");
      }
   }

}
