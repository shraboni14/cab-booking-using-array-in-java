package arrayobj.car.uber;

import java.util.Scanner;

import static arrayobj.car.uber.MainOperation.allOptions;
import static arrayobj.car.uber.RiderService.*;

public class RiderUserOperation {
    static Scanner scanner =  new Scanner(System.in);

    static Rider registerRider(){
        scanner.nextLine();
        System.out.println("Enter Rider ID : ");
        String riderId = scanner.nextLine();
        System.out.println("Enter Rider Name : ");
        String rierName = scanner.nextLine();
        System.out.println("Enter Source Location : ");
        String sourceLocation = scanner.nextLine();
        System.out.println("Enter Destination Location : ");
        String destinationLocation = scanner.nextLine();
        String IsBooked = "NO";
        String DriverId = "Not Assigned";
        String driverName = "Not Assigned";

        return new Rider(riderId,rierName,sourceLocation,destinationLocation,IsBooked,DriverId,driverName);
    }
    static void cancelCab(){
        System.out.println("Enter your Rider ID");
        scanner.nextLine();
        String riderIdToDelete = scanner.nextLine();

        for (int i =0; i<rIndex ; i++){
            if (riders[i].getRiderId().equals(riderIdToDelete)){
                for (int j = i; j<rIndex ; j++){
                    riders[j] = riders[j+1];
                }
                System.out.println("Booking cancel Successfully");
                rIndex = rIndex-1;
                break;
            }
        }
        for (int i =0; i<rIndex; i++){
            System.out.println("Rider ID : " + riders[i].getRiderId());
            System.out.println("Rider Name :" + riders[i].getRiderName());
            System.out.println("Rider Source Location :" + riders[i].getRiderSourceLocation());
            System.out.println("Rider Destination Location :" + riders[i].getRiderDestinationLocation());
            System.out.println("Car Booking Time :" + riders[i].getBookingTime());
            System.out.println("Is Accepted by driver: "+ riders[i].getIsBooked());
            System.out.println("Assigned Driver ID: "+ riders[i].getDriverId());
            System.out.println("Assigned Driver Name: "+ riders[i].getDriverName());
            System.out.println("-------------------------------");
        }
    }
    public static void riderFunctionality(){

        int choice;
        while (true){
            System.out.println("Press 1 : For Booking a CAB\n " +
                    "Press 2 : For Show Booking\n " +
                    "Press 3 : For Cancel CAB\n" +
                    "Press 4 : For main menu\n" );
            System.out.println("Enter Your Choice");
            choice = scanner.nextInt();

            switch (choice){
                case 1:
                    Rider rider = registerRider();
                    addRider(rider);
                    break;
                case 2: getAllRiders();
                        break;
                case 3:
                    cancelCab();
                    break;
                case 4:
                    allOptions();
                    break;

            }
        }

    }
}
